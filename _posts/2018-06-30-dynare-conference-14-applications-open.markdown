---
layout: posts
title:  "14th Dynare Conference"
date:   2018-04-30
categories:
  - events
tags:
  - conference
---

Submission for the 14th Dynare Conference at ECB, Frankfurt, Germany, is now open.

See details [here][1]

[1]: http://www.dynare.org/events/14th-dynare-conference

