---
layout: posts
title:  "Dynare Summer School 2020"
date:   2020-03-31
categories:
  - events
tags:
  - summer school
---

**Update** Due to the coronavirus outbreak the Summer school is
canceled, see announcement
[here](https://forum.dynare.org/t/dynare-summer-school-2020/15110/15).

Dynare Summer School 2020 will be hosted from July 6-10, 2020 by the Banque de
France in Paris, France.

## Goals of the Summer School

The school will provide an introduction to Dynare and to Dynamic Stochastic
General Equilibrium (DSGE) modeling.

The courses will focus on the simulation and estimation of DSGE models with
Dynare. The school will also be the occasion to introduce the next official
major release of Dynare (version 4.6).

The Dynare Summer School is aimed at beginners as well as at more experienced
researchers. PhD students are encouraged to participate. Time will be devoted
to the (Dynare related) problems encountered by students in their research.

## Preliminary program

[Preliminary Program (pdf)](/assets/summer-school/2020/preliminary_program.pdf)

## Application

Interested parties can apply by sending a CV and a recent research paper (PDFs
only) to [school@dynare.org](mailto:school@dynare.org).

Applications should be submitted no later than April 15, 2020. We will confirm
acceptance by April 30, 2020.

People working in organizations that are members of DSGE-net (Bank of Finland,
Banque de France, Capital Group, European Central Bank, Norges Bank, Sverige
Riksbank, Swiss National Bank) should register via their network
representative.

## Registration Fee

The registration fee (which includes lunches, coffee breaks, and one diner) is
€250 for academics and €1750 for institutional members.

## Invited Speaker

- [Edward Herbst](http://edherbst.net/)
  (Federal Reserve Board of Governors)

The working title of Edward’s presentation is “Advanced Monte Carlo Techniques
and the Estimation of DSGE Models.”

## Animators

- [Stéphane Adjemian](https://stepan.adjemian.eu/) (Université du Maine)
- [Michel Juillard](https://ideas.repec.org/e/pju1.html#research) (Banque de France)
- [Frédéric Karamé](http://f.karame.free.fr/index.php) (Université du Maine)
- [Marco Ratto](https://ideas.repec.org/f/pra217.html#research) (European Commission Joint Research Centre)
- [Sébastien Villemot](https://sebastien.villemot.name/) (CEPREMAP)

This workshop is organized with the support of CEPREMAP, Banque de France, and
DSGE-net.

## Venue

Banque de France<br />
31 Rue Croix des Petits Champs<br />
75001 Paris<br />
France

## Workshop Organization

This is a laptop only workshop. Each participant is required to come with
his/her laptop with MATLAB R2009b or later installed. We will provide WiFi
access, but participants shouldn’t rely on it to access a MATLAB license server
at their own institution. As an alternative to MATLAB, it is possible to use
GNU Octave (free software, compatible with MATLAB syntax).

