---
layout: posts
title:  "Dynare Conference 2022"
date:   2021-12-20
categories:
  - events
tags:
  - Dynare conference
---

The 16th annual Dynare Conference will be held in person at the
[Lancaster University](https://www.lancaster.ac.uk/), UK from 30th June to 1st
July 2022.

The conference is organized by the Department of Economics of Lancaster
University Management School (LUMS), together with Banque de France, DSGE-net,
and the Dynare project at CEPREMAP.

Keynote speakers of this conference edition will be Jan Eeckhout (University of
Pompeu Fabra) and Silvana Tenreyro (London School of Economics, Bank of
England). Both the Keynote speakers will hold plenary sessions.

All papers should be submitted electronically using our [submission
form](https://docs.google.com/forms/d/e/1FAIpQLSeNWxrnFmIMUtT9-28Fm48cnDmSnOAoz0N9sre7h7tzslLRuA/viewform)
no later than midnight UK time on 6th March 2022.

Important dates:

 - 6th March 2022: deadline for paper submissions
 - 15th April 2022: authors of accepted papers will be informed

The Dynare conference will feature the work of leading scholars in dynamic
macroeconomic modeling and provide an excellent opportunity to present your
research results.

Topics of this conference edition will focus on:

 - Heterogeneity in households, firms, information, expectations, banks,
   housing, and financial intermediaries
 - Structural Changes due to Market Power, Automation, Digitalization,
   Productivity
 - Monetary and Fiscal Policies in models with Disaster Shocks, Covid Shock,
   Uncertainty Shocks. Inequality, Non-linearities, and Asymmetries.

Submissions of papers dealing with different aspects of General Equilibrium
modeling and computational methods are also welcomed. Papers using software
tools other than Dynare and theoretical contributions are also encouraged.

Accepted papers will be automatically considered for publication in the [Dynare
Working Paper series](https://www.dynare.org/wp/) conditional on the agreement of the submitter. Note that
publication in the Dynare Working Paper series does not prohibit submission to
another working paper series.

More information is available on the [conference
website](https://wp.lancs.ac.uk/dynare2022/). For any requests please contact
[dynare2022@lancaster.ac.uk](mailto:dynare2022@lancaster.ac.uk)
