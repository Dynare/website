---
layout: posts
title:  "Dynare Summer School 2022"
date:   2022-02-14
categories:
  - events
tags:
  - Dynare summer school
---

The Dynare Summer School 2022 will take place in person from May 30 to June
3, 2022. The event will be hosted by École Normale Supérieure (Paris).

## Goals of the Summer School

The school will provide an introduction to Dynare and to Dynamic Stochastic
General Equilibrium (DSGE) modeling.

The course will focus on the simulation and estimation of DSGE models with
Dynare. The school will also be the occasion to present the new features in the last
major release of Dynare (version 5).

The Dynare Summer School is aimed at beginners as well as at more experienced
researchers. PhD students are encouraged to participate.

## Preliminary program

TBD

## Application

Interested parties can apply by sending a CV and a recent research paper (PDFs
only) to [school@dynare.org](mailto:school@dynare.org).

Applications should be submitted no later than March 27, 2022. We will confirm
acceptance by April 1, 2022.

People working in organizations that are members of DSGE-net should register
via their network representative.

## Registration Fee

The registration fee (which includes lunches, coffee breaks, and one diner) is
€200 for academics and €1500 for institutional members.

## Animators

- [Stéphane Adjemian](https://stepan.adjemian.eu/) (Université du Maine)
- [Michel Juillard](https://ideas.repec.org/e/pju1.html#research) (Banque de France)
- [Willi Mutschler](https://mutschler.eu/) (University of Tuebingen)
- [Johannes Pfeifer](https://www.unibw.de/makro/mitarbeiter/johannespfeifer) (Universität der Bundeswehr München)
- [Marco Ratto](https://ideas.repec.org/f/pra217.html#research) (European Commission Joint Research Centre)
- [Sébastien Villemot](https://sebastien.villemot.name/) (CEPREMAP)

## Venue

École Normale Supérieure<br />
48 boulevard Jourdan<br />
75014 Paris<br />
France

## Workshop Organization

This is a laptop only workshop. Each participant is required to come with
his/her laptop with MATLAB R2014a or later installed. We will provide WiFi
access, but participants shouldn't rely on it to access a MATLAB license server
at their own institution. As an alternative to MATLAB, it is possible to use
GNU Octave (free software, compatible with MATLAB syntax).
