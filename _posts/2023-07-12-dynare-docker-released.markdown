---
layout: posts
title:  "Dynare Docker Containers Released"
date:   2023-07-12
related: true
categories:
  - new-dynare-release
tags:
  - release notes
---

We are pleased to announce the release of pre-configured Docker containers for Dynare on [Docker Hub](https://hub.docker.com/r/dynare/dynare).
The containers include both Octave and MATLAB pre-configured with Dynare already in the PATH and all recommended toolboxes. These containers are ideal for using Dynare in CI/CD environments ([example Workflow for GitHub Actions](https://github.com/wmutschl/DSGE_mod/tree/master/.github/workflows)) or High Performance Computing clusters with either [Docker, ENROOT or Singularity](https://wiki.bwhpc.de/e/BwUniCluster2.0/Containers).

To minimize maintenance efforts while ensuring high levels of security, reliability, and performance, our Docker containers are built from the official [MATLAB container base image](https://hub.docker.com/r/mathworks/matlab) using a custom [Dockerfile](https://git.dynare.org/Dynare/dynare/-/blob/master/docker/Dockerfile).
Additionally, we provide an example [docker-compose file](https://git.dynare.org/Dynare/dynare/-/blob/master/docker/docker-compose.yml) for complete access to the Ubuntu Desktop via VNC.

More information and instructions for customizations are available [here](https://git.dynare.org/Dynare/dynare/-/tree/master/docker).

## Supported tags (newer versions will be added in the future)

| Tags   | Dynare Version | MATLAB® Version | Octave Version | Operating System | Base Image              |
|--------|----------------|-----------------|----------------|------------------|-------------------------|
| latest | 5.4            | R2023a          | 5.2.0          | Ubuntu 20.04     | mathworks/matlab:R2023a |
| 5.4    | 5.4            | R2023a          | 5.2.0          | Ubuntu 20.04     | mathworks/matlab:R2023a |
| 5.3    | 5.3            | R2022b          | 5.2.0          | Ubuntu 20.04     | mathworks/matlab:R2022b |
| 5.2    | 5.2            | R2022a          | 5.2.0          | Ubuntu 20.04     | mathworks/matlab:R2022a |
| 5.1    | 5.1            | R2022a          | 5.2.0          | Ubuntu 20.04     | mathworks/matlab:R2022a |
| 5.0    | 5.0            | R2021b          | 5.2.0          | Ubuntu 20.04     | mathworks/matlab:R2021b |
| 4.6.4  | 4.6.4          | R2021a          | 5.2.0          | Ubuntu 20.04     | mathworks/matlab:R2021a |
