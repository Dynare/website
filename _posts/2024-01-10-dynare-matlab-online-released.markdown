---
layout: posts
title:  "Dynare now available for MATLAB Online"
date:   2024-01-10
related: true
categories:
  - new-dynare-release
tags:
  - release notes
---

We are pleased to announce the release of a Dynare package for [MATLAB
Online](https://www.mathworks.com/products/matlab-online.html).

No installation on the local computer is needed to use this package.
Everything takes place within the web browser.

Dynare for MATLAB Online can be downloaded from the dedicated tab on [the
Dynare download page](https://www.dynare.org/download/).

Running Dynare through MATLAB Online can for example be useful:
- when one does not have local administrator rights on their computer;
- when working from different computers (e.g. at home and at work), since no
  file synchronization is needed;
- when teaching to a large pool of students, since it gives an easy and uniform
  installation method.

The Dynare Team would like to thanks the MathWorks company, and in particular
Eduard Benet Cerda, for providing excellent technical support and help
throughout the process of creating this package.
