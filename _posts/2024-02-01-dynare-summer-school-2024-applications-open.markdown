---
layout: posts
title:  "Dynare Summer School 2024"
date:   2024-02-01
categories:
  - events
tags:
  - summer school
---

The Dynare Summer School 2024 will take place in person from May 27 to May
31, 2024. The event will be hosted by École Normale Supérieure (Paris).

## Goals of the Summer School

The school will provide an introduction to Dynare and to Dynamic Stochastic
General Equilibrium (DSGE) modeling.

The course will focus on simulation and estimation of DSGE models with
Dynare.  The school will also be an opportunity to introduce new
features in Dynare 6.

This Summer school is aimed at beginners as well as at more experienced
researchers. PhD students are encouraged to participate. Time will be devoted to
the (Dynare related) problems encountered by students in their research.

## Application

Interested people should apply by sending an email to school@dynare.org.

Application should be done no later than March 17, 2024. You will have to
attach a CV and a recent research paper (please only send PDFs). We will confirm
acceptance the following week (no later than March 22th, 2024).

## Registration Fee

Registration fee (including lunches, coffee breaks and one diner): € 400 for
academics and € 1800 for institutional members.

## Workshop Animators

- [Stéphane Adjemian](https://stephane-adjemian.fr) (Université du Mans)
- [Michel Juillard](https://ideas.repec.org/e/pju1.html#research) (Banque de France)
- [Frédéric Karamé](http://f.karame.free.fr/index.php) (Université du Mans)
- [Willi Mutschler](https://mutschler.eu/) (University of Tuebingen)
- [Johannes Pfeifer](https://sites.google.com/site/pfeiferecon/) (University of the Bundeswehr Munich)
- [Marco Ratto](https://ideas.repec.org/f/pra217.html#research) (European Commission Joint Research Centre)
- [Normann Rion](https://normannrion.com/) (CEPREMAP)
- [Sébastien Villemot](https://sebastien.villemot.name/) (CEPREMAP)

This workshop is organized with the support of Banque de France, CEPREMAP, and DSGE-net.

## Preliminary program

Will be available soon.

## Venue

École Normale Supérieure<br />
48 boulevard Jourdan<br />
75014 Paris<br />
France

## Social event

On Thursday evening, after a visit to the most famous museum of Impressionist painters in Paris, there will be a dinner at the [Musée d'Orsay](https://www.musee-orsay.fr/en).

## Workshop Organization

This is a laptop only workshop. Each participant is required to come with
their laptop with MATLAB R2018b or later installed. We will provide WiFi
access, but participants shouldn’t rely on it to access a MATLAB license server
at their own institution. As an alternative to MATLAB, it is possible to use
GNU Octave (free software, compatible with MATLAB syntax).
