---
layout: posts
title:  "Dynare in Julia available for testing"
date:   2024-03-14
related: true
categories:
  - new-dynare-release
tags:
  - julia
---

[Dynare.jl](https://github.com/DynareJulia/Dynare.jl), an alternative
implementation of Dynare in Julia, is now ready for testing by users.

The following features are implemented:
- steady state;
- local approximation of the solution at 1st and 2nd order;
- perfect foresight simulations;
- forecasting;
- optimal policy.

[Julia](https://julialang.org/) is an open source programming language. It is recommended to use it with [VScode](https://code.visualstudio.com/). See also [Julia in Visual Studio Code](https://code.visualstudio.com/docs/languages/julia).

Documentation is available at: [https://DynareJulia.github.io/Dynare.jl](https://DynareJulia.github.io/Dynare.jl).

Installation in Julia can be done with the following commands:
```
julia> using Pkg
julia> pkg"add Dynare"
```

Thank you for reporting any error, question or desired additional feature on
the [Dynare forum](https://forum.dynare.org/) or as a [GitHub
issue](https://github.com/DynareJulia/Dynare.jl/issues).
