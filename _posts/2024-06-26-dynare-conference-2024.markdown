---
layout: posts
title:  "Dynare Conference 2024"
date:   2024-06-26
categories:
  - events
tags:
  - Dynare conference
---

The 18th annual Dynare Conference will be held in person at the
[National University of Singapore](https://nus.edu.sg/) from December
12-13, 2024 in Singapore. The conference is organized by the
[Department of Economics](https://fass.nus.edu.sg/ecs/) at the
National University Singapore, the [Monetary Authority of
Singapore](https://www.mas.gov.sg/), together with
[DSGE-net](https://www.dsge.net/), and the [Dynare
project](https://www.dynare.org) at [CEPREMAP](https://cepremap.fr).

The keynote speakers will be [Eric
Leeper](https://uva.theopenscholar.com/eric-leeper) (University of
Virginia) and [Todd
Schoellman](https://sites.google.com/view/toddschoellman/) (Federal
Reserve Bank of Minneapolis).

We welcome theoretical, empirical, and quantitative submissions on all
topics within macro. We seek papers using different types of software
tools besides Dynare.

All papers should be submitted via the [submission form](https://docs.google.com/forms/d/1X3N0jtkk73bFKpBHMILLwT0nSqUDkwerQHkYmRfLyBg/). The deadline
for submissions is ~~August~~ September 15th, 2024 and notifications of acceptance
will be sent by September 26th, 2024. There is no fee to attend, lunch
on both days and a conference dinner will be provided. Authors of
accepted papers will be responsible for their airfare and
accommodation.

For any other requests, please contact: [conference@dynare.org](mailto:conference@dynare.org).

[Call for papers](https://dynare.adjemian.eu/dynare-conference-2024.pdf)

[Program](/assets/conference/2024/program.pdf)
