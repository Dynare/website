---
layout: posts
title:  "Dynare Conference 2025"
date:   2025-02-06
categories:
  - events
tags:
  - Dynare conference
---


The 19th Annual Dynare Conference will be held in person in Helsinki,
Finland on June 5-6, 2025.  The conference is organized by the Bank of
Finland together with DSGE-net, and the Dynare project at
CEPREMAP. The conference’s special theme is **Heterogeneity and
Nonlinearities in Macroeconomics**.  The keynote speakers will be
[Matteo Iacoviello](https://www.matteoiacoviello.com/) (Federal
Reserve Board) and [Leonardo
Melosi](https://sites.google.com/site/lemelosi/) (University of
Warwick).


We invite submissions of theoretical, empirical, and quantitative
research on macroeconomic topics.  Following this year’s special
theme, we are particularly interested in contributions that explore
heterogeneity and non-linearities in quantitative macroeconomic
models. We encourage the use of diverse software tools beyond Dynare.


All papers should be submitted via the [submission
form](https://www.lyyti.fi/reg/SubmissionsDynare2025). The deadline
for submissions is March 16th, 2025, and notifications of acceptance
will be sent by April 11th, 2025. There is no fee to attend, lunch on
both days and a conference dinner will be provided. Authors of
accepted papers will be responsible for their airfare and
accommodation.


**Scientific committee**: Stéphane Adjemian (Université du Mans and
Dynare Team), Johannes Pfeifer (Universität der Bundeswehr München and
Dynare Team), Annukka Ristiniemi (European Central Bank), Juha
Kilponen (Bank of Finland), Esa Jokivuolle (Bank of Finland) and
Markus Haavio (Bank of Finland) For inquiries, please contact
[conference(at)dynare.org](mailto:conference@dynare.org) and on local organisation, please contact
Riikka Virtanen, [riikka.virtanen(at)bof.fi](mailto:riikka.virtanen@bof.fi)


[Call for papers](https://dynare.adjemian.eu/dynare-conference-2025.pdf)
