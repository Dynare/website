#!/bin/bash

# Reads the dynare-*.rdf files in the current directory, and then creates a
# wp.yml suitable for consumption by Jekyll

declare -A wp_authors wp_title wp_year wp_url wp_data_url wp_source_url

# Parses an entry “Fieldname: value”, stored as a single line in $field
parse_field ()
{
    local firstname lastname

    if [[ $field =~ ^Number:\ +([0-9]+) ]]; then
        number=${BASH_REMATCH[1]}
    elif [[ $field =~ ^Creation-Date:\ +([0-9]+) ]]; then
        year=${BASH_REMATCH[1]}
    elif [[ $field =~ ^Title:\ +(.*) ]]; then
        title=${BASH_REMATCH[1]}
    elif [[ $field =~ ^Author-Name:\ +(.+) ]]; then
        if [[ -n $authors ]]; then
            authors+=", "
        fi
        lastname=${BASH_REMATCH[1]%%, *}
        firstname=${BASH_REMATCH[1]#*, }
        authors+="$firstname $lastname"
    elif [[ $field =~ ^File-URL:\ +(.+) ]]; then
        last_url=${BASH_REMATCH[1]}
    elif [[ $field =~ ^File-Function:\ +(.+) ]]; then
        if [[ ${BASH_REMATCH[1]} == "Main text" ]]; then
            url=$last_url
        elif [[ ${BASH_REMATCH[1]} == "Data used in the paper" ]]; then
            data_url=$last_url
        elif [[ ${BASH_REMATCH[1]} == *[Ss]ource\ code* ]]; then
            source_url=$last_url
        else
            echo "Unrecognized value for File-Function field: ${BASH_REMATCH[1]}!" >&2
            exit 1
        fi
    fi
    unset field
}

# Add a new template (i.e. a working paper in RePEc’s jargon) to the global arrays
add_template ()
{
    echo "Found working paper no. $number"
    wp_authors[$number]=$authors
    wp_title[$number]=$title
    wp_year[$number]=$year
    wp_url[$number]=${url:-$last_url} # If the File-Function field is missing, then the last URL is the main text
    wp_data_url[$number]=$data_url
    wp_source_url[$number]=$source_url
    unset number authors title year url data_url source_url
}

# Parses an RDF file given on standard input
parse_rdf_file ()
{
    local line last_url field
    local number authors title year url data_url source_url

    while IFS= read -r line; do
        if [[ $line =~ ^\ *$ ]]; then
            # Empty line, add the template and jump to next line
            if [[ -n $field ]]; then
                parse_field
            fi
            if [[ -n $number ]]; then
                add_template
            fi
            continue
        fi
        if [[ $line =~ ^\ + ]]; then
            # Continuation of a field
            field+=$line
        else
            if [[ -n $field ]]; then
                parse_field
            fi
            field=$line
        fi
    done

    # Handle the case when there is no newline at the end of the file
    if [[ -n $field ]]; then
        parse_field
    fi
    if [[ -n $number ]]; then
        add_template
    fi
}

for f in dynare-*.rdf; do
    echo "Parsing $f…"
    parse_rdf_file < "$f"
    echo
done

if ((${#wp_authors[@]} == 0)); then
    echo "No working paper found!" >&2
    exit 1
else
    echo "Found a total of ${#wp_authors[@]} working papers."
fi

for ((i = ${#wp_authors[@]}; i >= 1; i--)); do
    if [[ -z ${wp_authors[$i]} || -z ${wp_title[$i]} || -z ${wp_year[$i]} || -z ${wp_url[$i]} ]]; then
        echo "Some field is missing in working paper $i!" >&2
        exit 1
    fi
    echo "- number: $i"
    echo "  authors: ${wp_authors[$i]}"
    echo "  title: \"${wp_title[$i]}\""
    echo "  year: ${wp_year[$i]}"
    echo "  url: ${wp_url[$i]}"
    if [[ -n ${wp_source_url[$i]} ]]; then
        echo "  source_url: ${wp_source_url[$i]}"
    fi
    if [[ -n ${wp_data_url[$i]} ]]; then
        echo "  data_url: ${wp_data_url[$i]}"
    fi
    echo
done > wp.yml
