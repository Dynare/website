---
title: Contact
---
<br />

Here are the various contact points of the Dynare Team. Please make sure to use
the one that best corresponds to your request.

- Our main support channel is the [Dynare forum](https://forum.dynare.org). All
  questions about the installation or use of Dynare should go there. More
  general questions related to macroeconomic modelling are also welcome.

- Bugs should be reported by opening a new issue on our [issue
  tracker](https://git.dynare.org/Dynare/dynare/-/issues). Note that you will
  have to create an account on our Gitlab instance. If you have encountered a
  problem with Dynare, but are unsure whether it is really a bug, please ask on
  the forum first before opening an issue.

- Small code contributions can directly be submitted via a new [merge
  request](https://git.dynare.org/Dynare/dynare/-/merge_requests). Large code
  contributions should first be discussed by writing to
  [dev@dynare.org](mailto:dev@dynare.org); beware, this is a publicly-archived
  mailing list.

- Inquiries about the Dynare Conference should go to
  [conference@dynare.org](mailto:conference@dynare.org).

- Inquiries about the Dynare Summer School should go to
  [school@dynare.org](mailto:school@dynare.org).

- Submissions to the Dynare Working Papers Series should be sent to
  [wp@dynare.org](mailto:wp@dynare.org).

- Violations of the [Dynare Code of
  Conduct](https://git.dynare.org/Dynare/dynare/-/blob/master/CODE_OF_CONDUCT.md)
  should be reported to [community@dynare.org](mailto:community@dynare.org).

- Requests that do not fall in any of the above categories should be sent to
  [contact@dynare.org](mailto:contact@dynare.org).
