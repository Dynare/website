---
title: Contributing
---
<br />

# How can you contribute to the Dynare Project ?

As a <i class="fas fa-external-link-alt"></i> [free and open-source
software](https://en.wikipedia.org/wiki/Free_and_open-source_software) project,
we think of Dynare as a community. Though primary development is undertaken to
varying degrees by a [core team](/about#team), we encourage others to be
involved in any way that interests them. That could be anything from attending
one of [our events](#come-to-one-of-our-events), to participating actively in
the [Dynare Forum](#participate-in-the-forum), to spotting and
[reporting bugs](#reporting-bugs), to directly making [changes to the
codebase](#code-development).

## Come to one of our events

Every year we organize three events: the Dynare Summer School, the Dynare
Conference and the Dynare Workshop for Advanced Users. These events are public
and their announcements are posted on this website.

## Participate in the forum

The [Dynare Forum](https://forum.dynare.org/) is a great resource to turn to
when you’re stuck. Along with the manual, it should be a point of reference
when working with Dynare.

As an advanced user, your contribution will be useful in several ways. First
and foremost, you’ll be helping out others who are just beginning on the same
path that you’re already on. Second, you may be able to bring a perspective to
the table that’s different than that of a beginner and that of someone on the
[core team](/about#team). Finally, your participation will help foster a larger
sense of community and will hopefully allow you gain a deeper understanding of
something you already understand well.

## Reporting bugs

If you find a bug, we want to know about it! But, first, before you tell us
about it, make sure it’s a real bug. Head over to the [Dynare
Forum](https://forum.dynare.org/) and search for the problem you’ve
encountered; maybe someone else has encountered the same issue and what you
think is a bug is actually intentional behavior. If there’s nothing on the
forum and you’re not certain it’s a bug, please post a question there. We’ll
see it and respond accordingly. If, however, you’re sure it’s a real bug,
please report it on our Gitlab Issue page. But! Before you do, ensure you are
working with the latest version of Dynare.

- If reporting a bug in the ***stable*** version of Dynare:
<p class="indent" markdown="1">Ensure the bug exists in the [latest stable
version of Dynare](/download#stable) telling us exactly how to reproduce the
problem.</p>
- If reporting a bug in the ***unstable*** version of Dynare:
<p class="indent" markdown="1">Ensure the bug exists in the most recent version
of the Dynare source code. You can either test it by installing the latest
[Dynare snapshot](/download#snapshot) or by compliing the source code from the
[Git repository](https://git.dynare.org/Dynare/dynare).</p>

If you have encountered a bug in the stable version of Dynare, there’s a chance
we have already fixed it. You can see a list of bugs fixed on the [fixed bugs
Wiki page](https://git.dynare.org/Dynare/dynare/wikis/FixedBugs). There’s also
a chance the bug has already been reported. You can see a list of reported bugs
by checking both the [known bugs Wiki
page](https://git.dynare.org/Dynare/dynare/-/wikis/Known-bugs-present-in-the-current-stable-version)
and the [Gitlab Issue
page](https://git.dynare.org/Dynare/dynare/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=bug).

Once you have ensured you’re working with the correct version of Dynare and
that this bug has not yet been reported, report the bug on our [Gitlab Issue
page](https://git.dynare.org/Dynare/dynare/issues), providing all the code
necessary (`.mod` files, `.m` files, data) to reproduce the bug. If working
with a larger `.mod` file, please pare it down as much as possible. This will
help us find the bug more quickly.

NB: You will need to create an account on our GitLab instance to report a bug.
Please be aware that account requests are manually validated, so be prepared to
wait for a couple of hours or days before your account is created; if your
account request is rejected, do not hesitate to [contact us](/contact)
directly.

## Code development

Detailed instructions on how to setup a local copy of the Dynare codebase and
propose your changes can be found in the
[CONTRIBUTING.md](https://git.dynare.org/Dynare/dynare/-/blob/master/CONTRIBUTING.md)
file in the git repository.

As Dynare is <i class="fas fa-external-link-alt"></i> [free
software](https://en.wikipedia.org/wiki/Free_and_open-source_software) licensed
under the <i class="fas fa-external-link-alt"></i> [GNU General Public License
(GPL)](https://www.gnu.org/licenses/gpl-3.0.en.html), any enhancements you make
to the Dynare codebase would need to be under GPL (or, in some specific
circumstances, under some other compatible license) so that it could be merged
back into Dynare.
